#! /usr/bin/env python

import os
from jinja2 import Environment,FileSystemLoader
from .xsd_classes import XSDSchema

def generate_file(input_xsd_file, template_file, output_file):

    namespaces={"s":"http://www.w3.org/2001/XMLSchema"}
    xsd_schema = XSDSchema( input_xsd_file, namespaces )

    template_dir = os.path.dirname(template_file)
    template_filename = os.path.basename(template_file)
    template = Environment(
        loader=FileSystemLoader(template_dir)
    ).get_template(template_filename)


    result = template.render(xsd_schema=xsd_schema)

    with open(output_file, "w") as text_file:
        text_file.write(result)
