#from lxml import etree
import re
from xml.etree import ElementTree as eTree
from copy import copy

def clean_string_from_namespace(input, namespaces):
    match_list = map( lambda x: "{"+x+"}", namespaces.values() )
    result = input
    for item in match_list:
        result = result.replace(item,'')
    return result

fortran_types_conversion = {
 "string": "CHARACTER(len=256)",
 "boolean": "LOGICAL",
 "double": "REAL(DP)",
 "integer": "INTEGER",
 "unsignedByte": "INTEGER",
 "nonNegativeInteger": "INTEGER",
 "positiveInteger": "INTEGER",
 "d2vectorType": "REAL(DP), DIMENSION(2)",
 "d3vectorType": "REAL(DP), DIMENSION(3)",
 "vectorType": "REAL(DP), DIMENSION(:), ALLOCATABLE",
 "doubleListType": "REAL(DP), DIMENSION(:), ALLOCATABLE",
 "matrixType":"REAL(DP), DIMENSION(:), ALLOCATABLE",
 "smearingChoiceType": "CHARACTER(len=256)",
 "integerListType": "INTEGER, DIMENSION(:), ALLOCATABLE",
 "integerVectorType": "INTEGER, DIMENSION(:), ALLOCATABLE",
 "constr_parms_listType": "REAL(DP), DIMENSION(4)",
 "d3complexDType": "REAL(DP), DIMENSION(6)",
 "disp_x_y_zType": "REAL(DP), DIMENSION(2)",
 "cell_dimensionsType": "REAL(DP), DIMENSION(6)",
}


class XSDSchema:
    def __init__(self, xsd_filename, namespaces):
        schema = eTree.parse(xsd_filename)
        self.target_namespace = schema.getroot().get("targetNamespace")
        xml_complex_types = schema.findall("s:complexType",namespaces=namespaces)
        self.complex_types = list(map(XSDType, xml_complex_types[:]))

        xml_simple_types = schema.findall("s:simpleType",namespaces=namespaces)
        self.simple_types = list(map(XSDType, xml_simple_types))

        self.construct_type_tree( )

    def construct_type_tree( self ):
        all_types =  self.types()
        for t in all_types:
            for tt in t.sequence:
                if tt.is_qes_type:
                    tt.xsd_type = list(filter(lambda x: x.name==tt.xsd_type_name, all_types ))[0]
            if (t.extension_is_qes_type == True):
                t.extension_xsd_type = list(filter(lambda x: x.name==t.extension_xsd_type_name, all_types ))[0]

    def sorted_complex_types( self ):
        return sorted( self.complex_types, key=lambda x: x.get_depth() )

    def types( self ):
        return self.simple_types + self.complex_types

class XSDType:
    NAMESPACES = {"s":"http://www.w3.org/2001/XMLSchema"}
    def __init__(self, xml_element):
        self.xsd_basic_type = clean_string_from_namespace( xml_element.tag, self.NAMESPACES )
        self.name = xml_element.get('name')

        elements = xml_element.findall(".//s:element",namespaces= self.NAMESPACES)
        self.sequence = list( map( XSDSequenceElement, elements ) )

        extensions = xml_element.findall(".//s:extension",namespaces= self.NAMESPACES)
        if len(extensions)>1:
            raise Exception(self.name+': more than one extension?!')
        if len(extensions)==1:
            self.extension_xsd_type_name = extensions[0].get("base").replace('qes:','')
            self.extension_is_qes_type = extensions[0].get("base").startswith("qes:")
        else:
            self.extension_xsd_type_name = None
            self.extension_is_qes_type = None
        self.extension_xsd_type = None

        restrictions = xml_element.findall(".//s:restriction",namespaces= self.NAMESPACES)
        if len(restrictions)>1:
            raise Exception(self.name+': more than one restriction?!')
        if (len(restrictions)==1) and ("base" in restrictions[0].attrib):
            self.restriction_xsd_type_name = restrictions[0].get("base").replace('qes:','')
            self.restriction_is_qes_type = restrictions[0].get("base").startswith("qes:")
        else:
            self.restriction_xsd_type_name = None
            self.restriction_is_qes_type = None
        self.restriction_xsd_type = None


        attrs = xml_element.findall(".//s:attribute",namespaces= self.NAMESPACES)
        self.attributes = list(map( XSDAttribute, attrs ))

    def read_function_name(self):
        if (self.xsd_basic_type == "complexType"):
            return "qes_read_"+self.name.replace('Type', '')
        else:
            return "extractDataContent"

    def bcast_function_name(self):
        return "qes_bcast_"+self.name.replace('Type', '')

    def init_function_name(self):
        if (self.name in ['matrixType', 'integerMatrixType']):
           return "qes_init_"+self.name.replace('Type','_1')+", "+\
                  "qes_init_"+self.name.replace('Type','_2')+", "+\
                  "qes_init_"+self.name.replace('Type','_3')
        elif (self.xsd_basic_type == "complexType" ):
           return "qes_init_"+self.name.replace('Type','')
        else:
           return None

    def write_function_name(self):
        if ( self.xsd_basic_type == "complexType" ):
           return "qes_write_"+self.name.replace('Type','')
        else:
           return "xml_addCharacters"

    def reset_function_name(self):
        if (self.xsd_basic_type == "complexType"):
           return "qes_reset_"+self.name.replace('Type','')
        else:
           return None

    def extension_fortran_type(self):
        if (self.extension_xsd_type_name == None):
            raise Exception('The complex type is not an extension!')
        else:
            return fortran_types_conversion[self.extension_xsd_type_name]
            
    def init_extension_fortran_type(self):
        tmp = re.sub(r'LEN=[\d]+', 'LEN=*',self.extension_fortran_type(),
              flags=re.IGNORECASE)
        return tmp.replace(', ALLOCATABLE','')

    def fortran_type_name(self):
        if (self.xsd_basic_type == "complexType"):
            return self.name.replace('Type', '')+"_type"
        elif (self.restriction_xsd_type_name != None):
            return fortran_types_conversion[self.restriction_xsd_type_name]
        else:
            return fortran_types_conversion[self.name]
    
    def init_fortran_type_name(self):
        tmp = re.sub(r'LEN=[\d]+', 'LEN=*',self.fortran_type_name(),
              flags=re.IGNORECASE)
        return tmp.replace(', ALLOCATABLE','')

    def is_simple(self):
        return self.xsd_basic_type=="simpleType"

    def is_extension(self):
        return self.extension_xsd_type_name != None

    def extension_type(self, types):
        items = list(filter(lambda x: x.name==self.extension_xsd_type_name,types))
        if len(items)>0:
            return items[0]
        else:
            return None

    def extension_attributes(self, types):
        if self.extension_xsd_type_name == None:
            return []
        items = list(filter(lambda x: x.name==self.extension_xsd_type_name,types))
        if len(items)>0:
            return items[0].attributes
        else:
            return []

    def has_multi_sequence(self):
        return any((element.max_occurs==-1) or (element.max_occurs>1) for element in self.sequence)

    def get_depth(self):
        if len(self.sequence) == 0:
            return 0
        else:
            item = max( self.sequence, key=lambda x: x.get_depth() )
            return item.get_depth()+1


    def init_argument_line(self, types):
        line_head = []
        line_tail = []
        indent = len('  SUBROUTINE qes_init_'+self.name.replace('Type',''))
        for attribute in self.attributes:
            if attribute.is_required:
                line_head.append(attribute.name)
            else:
                line_tail.append(attribute.name)
        for attribute in self.extension_attributes(types):
            if attribute.is_required:
                line_head.append(attribute.name)
            else:
                line_tail.append(attribute.tail)
        for element in self.sequence:
            if element.min_occurs != 0:
                line_head.append(element.tag_name)
            else:
                line_tail.append(element.tag_name)
        if self.is_extension():
            line_head.append(self.name.replace('Type',''))
        line = 'obj, tagname'
        lines=[]
        max_line = 90
        arglist = line_head + line_tail
        lastindex = len(line_head+line_tail) - 1
        for arg in line_head+line_tail:
            if len(line)+ indent >  max_line and arglist.index(arg)< lastindex:
                line = line+',&'
                lines.append(line)
                line=''
            if line=='':
                line=indent * ' '+arg
                max_line = 90 + indent
            else:
                line +=', '+arg
        max_line = 100
        if  len(line) > max_line:
            line +=' &'
            lines.append(line)
            lines.append(indent*' ')
        else:
            lines.append(line)
        return str.join('\n',lines)



class XSDSequenceElement:
    def __init__(self, xml_element):
        self.tag_name = xml_element.get("name")
        if "default" in xml_element.keys():
            self.default = xml_element.get("default")
        else:
            self.default = None
        self.is_qes_type = xml_element.get("type").startswith("qes:")
        self.xsd_type_name = xml_element.get("type").replace('qes:','')
        self.xsd_type = None
        if "minOccurs" in xml_element.keys():
            self.min_occurs = int( xml_element.get("minOccurs") )
        else:
            self.min_occurs = 1
        if "maxOccurs" in xml_element.keys():
            if (xml_element.get("maxOccurs")!="unbounded"):
                self.max_occurs = int( xml_element.get("maxOccurs") )
            else:
                self.max_occurs = -1
        else:
            self.max_occurs = 1

    def fortran_type_name(self):
        if (self.is_qes_type):
            return self.xsd_type.fortran_type_name()
        else:
            return fortran_types_conversion[self.xsd_type_name]
    
    def init_fortran_type_name(self):
        tmp = re.sub(r'LEN=[\d]+', 'LEN=*',self.fortran_type_name(),
              flags=re.IGNORECASE)
        return tmp.replace(', ALLOCATABLE','')

    def get_depth(self):
        if (self.xsd_type == None):
            return 0
        else:
            return self.xsd_type.get_depth()

    def optional(self):
        if (self.min_occurs==0):
            return('OPTIONAL,')
        return ''

    def dimension(self):
        if (self.max_occurs != 1):
            if (self.min_occurs == self.max_occurs):
                return('DIMENSION({}),'.format(self.max_occurs))
            else:
                return('DIMENSION(:),')
        return ''



class XSDAttribute:
    def __init__(self, xml_element):
        self.name = xml_element.get("name")
        if "default" in xml_element.keys():
            self.default = xml_element.get("default")
        else:
            self.default = None
        if "type" in xml_element.keys():
            self.xsd_type_name = xml_element.get("type").replace('qes:','')
        else:
            self.xsd_type_name = "string"
        if "use" in xml_element.keys():
            self.is_required = xml_element.get("use")=="required"
        else:
            #self.is_required = False
            self.is_required = True

    def fortran_type_name(self):
        return fortran_types_conversion[self.xsd_type_name]
    
    def init_fortran_type_name(self):
        tmp = re.sub(r'LEN=[\d]+', 'LEN=*',self.fortran_type_name(),
              flags=re.IGNORECASE)
        return tmp.replace(', ALLOCATABLE','')
